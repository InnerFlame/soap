<?php
// Алготитм действий на клиенте
// создаем SOAP клиент
// посылаем SOAP запрос и получаем результат


$client = new SoapClient('http://soap/stock.wsdl');
try {
    $result = $client->getStock('10');//если такого товара нет, исключение
    echo $result;
} catch (SoapFault $e){
    echo $e->getMessage();
}
try {
    $result = $client->getStock('2');//если есть такой товар
    echo $result;
} catch (SoapFault $e){
    echo $e->getMessage();
}
