<?php
// Алготитм действий на сервере
// создаем функцию/метод веб сервиса, в зависимости от того процедурный или обьектный подход
// отключаем хэширование WSDL документа
// создаем SOAP сервер
// добавляем метод/функцию к серверу
// запускаем сервер

# object
class StockService
{
    function getStock($num)
    {
        $arr = array(
            '1' => 100,
            '2' => 200,
            '3' => 300
        );

        if (isset($arr[$num])) {
            return $arr[$num];
        } else {
            throw new SoapFault('server', 'no goods');
        }

    }
}
ini_set('soap.wsdl_cache_enabled', '0');
$server = new SoapServer('http://soap/stock.wsdl');
$server->setClass('StockService');
$server->handle();


// procedured
//<?php
//
//    function getStock($num)
//    {
//        $arr = array(
//            '1' => 100,
//            '2' => 200,
//            '3' => 300
//        );
//
//        if (array_key_exists($num, $arr)) {
//            return $arr[$num];
//        } else {
//            return 0;
//        }
//
//    }
//
//ini_set('soap.wsdl_cache_enabled', '0');
//$server = new SoapServer('http://soap/stock.wsdl');
//$server->addFunction('getStock');
//$server->handle();
//--------------------
//если неесколько функций нужно не забыть описать эти функции в WSDL в operation
//$arr = array('f1', 'f2');
//$server->addFunction($arr);
//$server->handle();
